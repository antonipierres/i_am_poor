import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
      backgroundColor: Colors.cyan,
      appBar: AppBar(
        title: const Text('I Am Poor'),
        backgroundColor: Colors.cyan[700]
      ),
      body: const Center(child: Image(image: AssetImage('images/carbon.png'),)),
    ),
  ));
}
